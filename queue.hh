#ifndef QUEUE_HH
#define QUEUE_HH

#include <iostream>

template <typename T>
class Queue{
private:
    struct Node {
        T data;
        Node *next;
    };
    Node *head;
    Node *tail;
    int count;
public:
    void append(T data);
    int dequeue(T &data);
    void dequeue();
    int front(T &data);
    int size();
    void clear();
    bool empty();
    //Queue();
    Queue(): head(NULL), tail(NULL), count(0){};
    ~Queue() = default;
};

#endif /* ifndef */
