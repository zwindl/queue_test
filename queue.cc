#include "queue.hh"
#include <iostream>

/*
 *template<typename T>
 *Queue<T>::Queue(): head(NULL), tail(NULL), count(0){}
 */

template<typename T>
bool Queue<T>::empty(){
    return count == 0;
}

template<typename T>
void Queue<T>::append(T data){
    auto *new_node = new Node;
    new_node->data = data;
    new_node->next = NULL;
    if(empty()){
        head = tail = new_node;
    }
    tail->next = new_node;
    tail = new_node;
    count ++;
}

template<typename T>
int Queue<T>::front(T &data){
    if(empty()){
        std::cerr << "Empty queue" << std::endl;
    } else {
        data = this->head->data;
        return 0;
    }
    return -1;
}

template<typename T>
void Queue<T>::dequeue(){
    if(!empty()){
        Node* tmp = head;
        if(--count == 0){
            tail = NULL;
        }
        head = head->next;
        delete tmp;
        return;
    }
}

template<typename T>
int Queue<T>::dequeue(T &data){
    if(front(data) == 0){
        dequeue();
        return 0;
    }
    return -1;
}

template<typename T>
void Queue<T>::clear(){
    T tmp;
    while(!empty()){
        dequeue(tmp);
    }
}

template<typename T>
int Queue<T>::size(){
    return this->count;
}
