#include "queue.hh"
#include <iostream>

using std::cin;
using std::cout;
using std::endl;

int main()
{
    Queue<int> victims;

    int count(0), step(0);
    cout << "Please input the victim's count: ";
    cin >> count;
    cout << "Please input the step: ";
    cin >> step;

    for(int i=1; i<=count; i++)
        victims.append(i);

    while(!victims.empty()){
        for(int i=0; i<step; i++){
            int a;
            victims.dequeue(a);
            victims.append(a);
        }
        int killed;
        victims.dequeue(killed);
        cout << killed << " was killed!" << endl;
    }

    return 0;
}
